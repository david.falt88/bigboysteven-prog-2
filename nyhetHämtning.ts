import Parser from 'rss-parser';

export async function hämtaNyhet(url:string,maxNyheter:number=5){
    const parser = new Parser();
    return (await parser.parseURL('https://www.svd.se/?service=rss')).items.slice(0,maxNyheter);
}
