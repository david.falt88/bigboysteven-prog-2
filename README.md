# 



## Install


```
$ npm install 
```

## Usage


```
Mitt program ska ta nyheter ifrån SvD med hjälp av parser och visa dem i min terminal med fina färger och punkteringar med hjälp av colors.
```

## API



### Functions



### Class



## License

GPL, see LICENSE file

## Contribute

Please feel free to open an issue or submit a pull request.

## Changelog

 * **0.0.1** *2020-??-??* First version published


## Author
David Fält
**© 202?-202? [??](https://??.??)**
