import colors from 'colors';
import moment from 'moment';

export function nyhetsText(news:any) {
    const time = moment(news.pubDate).format("YYYY-MM-DD HH:mm");
        let result = colors.white.bold(" ○ ");
        result += colors.blue.bold(time)+" ";
        result += colors.cyan.bold(news.title)+"\n";
        return result;
}