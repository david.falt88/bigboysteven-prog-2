import {nyhetsText} from "../src/nyhetVisning";
import assert from 'assert';
import stripAnsi from 'strip-ansi';

describe('Enkelt Test', function() {
    describe('Test', function(){
        it('This should be true', function() {
            assert.strictEqual(true, true);
          });
    });
});  

describe('Test av nyhetVisning', function() {
    const news:any = {};
    news.pubDate = "2021-12-02 20:21"
    news.title = "Detta är ett test";
    describe('Test', function(){
        const namn_av_test = nyhetsText(news);
        it('Detta bör vara sant!', function() {
            assert.strictEqual(stripAnsi(namn_av_test),
            " ○ 2021-12-02 20:21 Detta är ett test\n");
        });
    });
});  
