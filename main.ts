#!/usr/bin/env node
import moment from 'moment';
import colors from 'colors';

import {hämtaNyhet} from "./nyhetHämtning";
import {nyhetsText} from "./nyhetVisning"

async function main() {
    
    console.log("\n"+colors.magenta.underline.bold("5 Senaste Nyheterna ifrån SvD"+"\n"));
    let news_list = await hämtaNyhet('https://www.svd.se/?service=rss');
    let result = "";
    for(let news of news_list) {
        result += nyhetsText(news);
    }
    console.log(result);
}

main();